import React, { Component } from "react";
import HeaderLayout from "../components/HeaderLayout";

function Header() {
    return (
        <>
            <HeaderLayout />
        </>
    );
}

export default Header;
