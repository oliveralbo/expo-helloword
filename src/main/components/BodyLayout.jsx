import React from "react";
import { Video } from "expo-av";
import functions from '../../../functions/functions'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    ScrollView,
} from "react-native";

//class Body extends Component {
function Body(props) {

    const msj = () => {
        functions.alert("estoy en el body")
    }


    return (
        <View style={styles.body}>
            <ScrollView>
                <TouchableOpacity>
                    <Text style={styles.textColor} onPress={msj}>
                        {props.title}
                    </Text>
                    <TextInput
                        placeholder='NOMBRE DE USUARIO'
                        placeholderTextColor='#819FF7'
                        maxLength={10}
                        style={styles.textinput}
                    // value=''
                    />
                </TouchableOpacity>

                <Video
                    // source={{
                    //   uri: "http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4",
                    // }}
                    source={{
                        uri: "http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4",
                    }}
                    rate={1.0}
                    volume={1.0}
                    isMuted={false}
                    resizeMode='cover'
                    shouldPlay={false}
                    isLooping
                    style={{ width: 300, height: 300 }}
                    marginTop={10}
                    useNativeControls={true}
                />
                <Video
                    source={{
                        uri: "http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4",
                    }}
                    // source={{
                    //   uri: "https://instagram.faep4-1.fna.fbcdn.net/v/t50.2886-16/96747033_525874758102302_5308951201696242872_n.mp4?_nc_ht=instagram.faep4-1.fna.fbcdn.net&_nc_cat=102&_nc_ohc=4n_TjF2nX6UAX-hnPgl&oe=5EDE7A61&oh=92cd070e935ebbcd816f548c9f784865",
                    // }}

                    rate={1.0}
                    volume={1.0}
                    isMuted={false}
                    resizeMode='cover'
                    shouldPlay={false}
                    isLooping
                    style={{ width: 300, height: 300 }}
                    marginTop={10}
                    useNativeControls={true}
                />
                <Video
                    // source={{
                    //   uri: "http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4",
                    // }}
                    source={{
                        uri: "http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4",
                    }}
                    rate={1.0}
                    volume={1.0}
                    isMuted={false}
                    resizeMode='cover'
                    shouldPlay={false}
                    isLooping
                    style={{ width: 300, height: 300 }}
                    marginTop={10}
                    useNativeControls={true}
                />
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },

    footer: {
        flex: 1,
        flexDirection: "row",
    },
    flex: {
        flex: 1,
    },
    textColor: {
        color: "white",
    },
    textinput: {
        borderWidth: 1,
        borderColor: "white",
        padding: 5,
        marginTop: 25,
    },
});

export default Body;
