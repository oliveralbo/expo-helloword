import React from "react";
import functions from '../../../functions/functions'
import {
    View,
    StyleSheet,
    Image,
    Button,
    Alert,

} from "react-native";

function Header() {

    const msj = () => {
        functions.alert("estoy en el header")
    }
    return (

        <View style={styles.header}>
            <View style={styles.headerLeft}>
                <Image
                    style={styles.logo}
                    source={{
                        uri:
                            "https://www.nicepng.com/png/detail/124-1240136_death-star-art-png.png",
                    }}
                />
            </View>
            <View style={styles.headerRight}>
                <Button title='Login' color='blue' onPress={msj}></Button>
            </View>
        </View>


    );

}

const styles = StyleSheet.create({

    header: {
        flex: 0.5,
        flexDirection: "row",
        marginTop: 30,
    },
    headerLeft: {
        flex: 1,
    },
    headerRight: {
        flex: 0.3,
        marginRight: 10,
    },

    logo: {
        width: 70,
        height: 70,

        borderRadius: 30,
    },

    flex: {
        flex: 1,
    },


    textinput: {
        borderWidth: 1,
        borderColor: "white",
        padding: 5,
        marginTop: 25,
    },
});

export default Header;
