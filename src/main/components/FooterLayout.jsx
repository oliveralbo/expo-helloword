import React from "react";

import {
    View,
    Text,
    StyleSheet,
    Image,

} from "react-native";

function Footer() {

    return (

        <View style={styles.footer}>
            <View style={[styles.flex, styles.footerLeft]}>
                <Text style={styles.textColor}>CIAPFA</Text>
            </View>
            <View style={[styles.flex, styles.footerCenter]}>
                <Text style={styles.textColor}>HOLA CIAPFA</Text>
            </View>
            <View style={[styles.flex, styles.footerRight]}>
                <Image
                    source={{
                        uri:
                            "https://cdn.dribbble.com/users/588874/screenshots/2249528/dribbble.png",
                    }}
                    style={styles.logo}
                ></Image>
            </View>
        </View>

    );

}

const styles = StyleSheet.create({
    logo: {
        width: 70,
        height: 70,

        borderRadius: 30,
    },
    footer: {
        flex: 1,
        flexDirection: "row",
    },
    flex: {
        flex: 1,
    },
    footerLeft: {
        alignItems: "center",
        justifyContent: "center",
    },
    footerCenter: {
        alignItems: "center",
        justifyContent: "center",
    },
    footerRight: {
        alignItems: "flex-end",
        justifyContent: "center",
    },
    textColor: {
        color: "white",
    },
    textinput: {
        borderWidth: 1,
        borderColor: "white",
        padding: 5,
        marginTop: 25,
    },
});

export default Footer;
