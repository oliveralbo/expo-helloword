import React, { Component } from "react";

import { StyleSheet, ImageBackground } from "react-native";
import Header from "./src/main/container/header";
import Body from "./src/main/container/body";
import Footer from "./src/main/container/footer";

class HolaCiapfa extends Component {
  render() {
    return (
      <ImageBackground
        source={{
          uri:
            "https://i.pinimg.com/originals/33/15/7e/33157ee3461bf2e3f1c3ba572e7d2c2f.png",
        }}
        style={styles.container}
      >
        <Header />
        <Body />
        <Footer />
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "green",
  },
});

export default HolaCiapfa;
